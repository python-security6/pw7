from flask import Flask, render_template, jsonify
from bs4 import BeautifulSoup
import requests
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry
import logging
logging.basicConfig(level=logging.INFO, filename="py_log.log",filemode="w")

app = Flask(__name__)
data = {'lots_els':[]}
lots_els= []

session = requests.Session()
retry = Retry(connect=3, backoff_factor=0.5)
adapter = HTTPAdapter(max_retries=retry)
session.mount('http://', adapter)
session.mount('https://', adapter)


@app.route("/")
def index():
    return jsonify(
        Hello = "world",
    )


@app.route("/pharmacy/advt/<advt>")
def get_by_advt(advt):
    url1 = "https://fms.ecc.kz/ru/announce/index/{s}?tab=lots".format(s=advt[0:-2])
    url2 = "https://fms.ecc.kz/ru/announce/index/{s}?tab=general".format(s=advt[0:-2])


    html_lots_text = session.get(url1,verify=False).text
    html_gen_text = session.get(url2,verify=False).text


    lots_soup = BeautifulSoup(html_lots_text, 'lxml')
    gen_soup = BeautifulSoup(html_gen_text, 'lxml')

    tables=gen_soup.find_all('table',class_='table table-bordered table-hover table-striped')
    beneficiary_name = tables[0].find_all('tr')[2].find('td').text
    beneficiary_bin = beneficiary_name.split()[0]
    beneficiary_name = beneficiary_name.replace(beneficiary_bin, ' ')
    data['beneficiary_bin'] = beneficiary_bin
    data['beneficiary_name'] = beneficiary_name


    inputs = lots_soup.find_all('input', class_='form-control')
    ad_name=inputs[1].get('value')
    status_name = inputs[2].get('value')
    start_date = inputs[4].get('value')
    end_date = inputs[5].get('value')
    data["ad_number"] = advt
    data["ad_name"] = ad_name
    data["status_name"] = status_name
    data["start_date"] = start_date
    data["end_date"] = end_date
    lots = lots_soup.find_all('tr')[1:]
    for lot in lots:
        lot_el = {}
        lot_number = lot.find('a', class_='btn-select-lot').text.replace(' ','')
        lot_name = lot.find_all('td')[3].get_text()
        amount = lot.find_all('td', class_='text-right')[1].text.replace(' ','')
        lot_el["lot_number"] = lot_number
        lot_el["lot_name"] = lot_name
        lot_el["amount"] = amount
        lots_els.append(lot_el)
    data["lots"] = lots_els
    return data
logging.info(data)

if __name__ == "__main__":
    app.run(port=5050,debug=True)